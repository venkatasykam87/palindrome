package com.devops.palindrome;

class PalindromeString{  
	public static void main(String args[]){  

		if(args.length > 1){
			System.out.println("Passed more than one string, pass only single string, considering given input as not a palindrome: FALSE");
		}else{
			String[] inputStrings = args[0].split(" ");
			if(inputStrings.length > 1){
				System.out.println("Passed more than one string, pass only single string, considering given input as not a palindrome: FALSE");
			}else{
				System.out.println("Passed one single string");
				String input=args[0];

				System.out.println("Input Value is: "+input);

				StringBuilder output = new StringBuilder();
				output.append(input);
				output.reverse();
				System.out.println("Output(reverse) value is: "+output);

				if(input.equals(output.toString())){
					System.out.println("This is palindrome String: TRUE");
				}else{
					System.out.println("This is not palindrome String: FALSE");
				}
			}
		}
	}  
}
